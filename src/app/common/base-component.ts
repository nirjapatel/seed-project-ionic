import { Component, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { LoadingController } from '@ionic/angular';
import { URLConstants } from '../common/route-link-constant';
import { callAPIConstants } from '../common/call-API-constant';

@Component({
    selector: 'base-Component',
    template: ''
})
export class BaseComponent {
    URLConstants = URLConstants;
    callAPIConstants = callAPIConstants;
    public router: Router;
    public nativeStorage: NativeStorage;
    public loadingController: LoadingController;
    constructor(public injector:Injector){
        this.router = injector.get(Router);
        this.nativeStorage=injector.get(NativeStorage);
        this.loadingController=injector.get(LoadingController);
    }
}