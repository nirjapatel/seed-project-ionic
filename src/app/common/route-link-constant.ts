export const URLConstants = {
    // public
    LOGIN: '/login-common',
    REGISTER: '/register',
    HOME:'/home',
    OTPPAGE:'/login-otp',
    NOTIFICATION: '/notification'
};