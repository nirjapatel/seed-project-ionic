import { Component, OnInit } from '@angular/core';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { Platform, AlertController } from '@ionic/angular';
@Component({
  selector: 'app-notification',
  templateUrl: './notification.page.html',
  styleUrls: ['./notification.page.scss'],
})
export class NotificationPage {

  public isAndroid:boolean =false;
public clickSub:any;
public notificationArray:any =[];
public arr :any;
  constructor(private localNotifications: LocalNotifications,public platform:Platform,public alertController:AlertController) {
    if(this.platform.is('android')){
      this.isAndroid =true;
    }
    
  }
  async presentAlert(data) {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: data,
      buttons: ['OK']
    });
    await alert.present();
  }
  unsub() {
    this.clickSub.unsubscribe();
  }
  notify(){
    this.clickSub = this.localNotifications.on('click').subscribe(data => {
      console.log(data);
      this.presentAlert('Your notifiations contains a secret = ' + data.data.secret);
      
      this.unsub();
    });
    
    this.localNotifications.schedule({
      id: 1,
      title:'Notification application',
      text: 'There is notification for You!!Please check',
      sound: this.isAndroid ?  'file://sound.mp3': 'file://beep.caf',
      data: { secret: "There is nofication for the Application" },
      foreground:true
    });
    this.localNotifications.on('trigger').subscribe(data => {
      console.log("The scheduled data is ",data);
      this.notificationArray.push(data.data.secret);
      console.log("Nofication array is ",this.notificationArray);
    })
  }

}
