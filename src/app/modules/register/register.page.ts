import { Component, OnInit, Inject, Injector } from '@angular/core';
import { BaseComponent } from '../common/base-component';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage extends BaseComponent implements OnInit {
  public firstname:any;
  public lastname:any;
  public username:any;
  public password:any;
  public mobilenumber:any;
  public test:boolean=true;
  constructor(public inj:Injector) { 
    super(inj);
  }

  ngOnInit() {
  }
  login(){
    this.router.navigate([this.URLConstants.LOGIN]);
  }
  register(){
    let registerData={
      firstname: this.firstname,
      mobilenumber: this.mobilenumber,
      lastname: this.lastname,
      username: this.username,
      password: this.password
    }
    console.log("Register value is ",registerData);
    //store in native storage of the device.
    this.nativeStorage.setItem("RegisterData",registerData).then((success)=>{
      console.log("Successfully register");
      //call an API for registration and navigate back to login.
      this.router.navigate([this.URLConstants.LOGIN]);
    })
  }

}
