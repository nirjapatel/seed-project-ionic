import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LoginOTPPage } from './login-otp.page';

describe('LoginOTPPage', () => {
  let component: LoginOTPPage;
  let fixture: ComponentFixture<LoginOTPPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginOTPPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LoginOTPPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
