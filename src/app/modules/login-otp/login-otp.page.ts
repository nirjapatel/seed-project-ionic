import { Component, OnInit, Injector } from '@angular/core';
import { BaseComponent } from '../common/base-component';

@Component({
  selector: 'app-login-otp',
  templateUrl: './login-otp.page.html',
  styleUrls: ['./login-otp.page.scss'],
})
export class LoginOTPPage extends BaseComponent implements OnInit {
public otpnumber: number;
  constructor(inj:Injector) {
    super(inj);
   }

  ngOnInit() {
  }
  ionViewDidEnter(){
    this.otpnumber = 123456;
  }
  loginwithOTP(){
    this.router.navigate([this.URLConstants.HOME]);
  }

}
