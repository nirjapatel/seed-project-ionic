import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LoginCommonPage } from './login-common.page';

describe('LoginCommonPage', () => {
  let component: LoginCommonPage;
  let fixture: ComponentFixture<LoginCommonPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginCommonPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LoginCommonPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
