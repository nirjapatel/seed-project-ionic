import { Component, OnInit, Injector } from '@angular/core';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { Facebook } from '@ionic-native/facebook/ngx';
import { SignInWithApple, AppleSignInResponse, AppleSignInErrorResponse, ASAuthorizationAppleIDRequest } from '@ionic-native/sign-in-with-apple/ngx';
import { BaseComponent } from '../common/base-component';
import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';
@Component({
  selector: 'app-login-common',
  templateUrl: './login-common.page.html',
  styleUrls: ['./login-common.page.scss'],
})
export class LoginCommonPage extends BaseComponent implements OnInit {

  public userData: any = {};
  public registerData: any;
  public username: any;
  public password: any;
  public user: boolean;
  public arr :any;
  public FB_APP_ID: number = 315345753190323;
  constructor(public inj: Injector,public faio:FingerprintAIO, private googlePlus: GooglePlus, private fb: Facebook,private signInWithApple: SignInWithApple) {
    super(inj);
  }

  ngOnInit() {
    
  }
  ionViewWillEnter(){
    this.user = true;
    this.arr =["arr1","arr2"];
  }
  ionViewDidEnter() {
    let options={
      title: 'Biometric Authentication', // (Android Only) | optional | Default: "<APP_NAME> Biometric Sign On"
      description: 'Place your finger near sensor to login', // optional | Default: null
      fallbackButtonTitle: 'Use Backup', // optional | When disableBackup is false defaults to "Use Pin".
                                         // When disableBackup is true defaults to "Cancel"
      disableBackup:true,  // optional | default: fals
      }
      this.nativeStorage.getItem('RegisterData').then((success) => {
        this.registerData = success;
        console.log("THe registered data saved is ", this.registerData);
        
          this.faio.show(options).then((result: any) => {
            console.log("Result for fingerprint is ",result);
            //condition for success and service call
            this.router.navigate(["/home"]);
          
        })
        this.username = this.registerData.username;
        this.password = this.registerData.password;
  
        //console.log("Username and password", this.username,this.password);
      })
  }
  sendOTP(){
    this.router.navigate([this.URLConstants.OTPPAGE]);
  }
  signup() {
    this.router.navigate([this.URLConstants.REGISTER]);
  }
  
  login() {
    console.log("Username and password", this.username,this.password);

    if ((this.username === this.registerData.username) && (this.password === this.registerData.password)) {
      console.log("Navigate ");

      this.router.navigate([this.URLConstants.HOME]);
    }
  }
  googlesignin() {
    console.log("sign in with google");
    this.googlePlus.login({})
      .then(result => {
        this.userData = result;
        console.log("Result is ", this.userData);
        this.router.navigate([this.URLConstants.HOME]);
      })
      .catch(err => {
        this.userData = `Error ${JSON.stringify(err)}`
        console.log("Error will google login")
      });
  }
  async facebooksignin() {
    const loading = await this.loadingController.create({
      message: 'Please wait...'
    });
    this.presentLoading(loading);
    let permissions = new Array<string>();

    //the permissions your facebook app needs from the user
    permissions = ['public_profile', 'user_friends', 'email'];

    this.fb.login(permissions)
      .then(response => {
        let userId = response.authResponse.userID;
        console.log("User Id is ", userId);

        //Getting name and gender properties
        this.fb.api("/me?fields=name,email", permissions)
          .then(user => {

            user.picture = "https://graph.facebook.com/" + userId + "/picture?type=large";
            //now we have the users info, let's save it in the NativeStorage
            console.log("User info", user.picture);

            this.nativeStorage.setItem('facebook_user',
              {
                name: user.name,
                email: user.email,
                picture: user.picture
              })
              .then(() => {
                this.router.navigate([this.URLConstants.HOME]);
                loading.dismiss();
              }, error => {
                console.log("NAtive storage error", error);
                loading.dismiss();
              })
          })
      }, error => {
        console.log("permission error", error);
        loading.dismiss();
      });
  }
  async presentLoading(loading) {
    return await loading.present();
  }
  applelogin(){
    this.signInWithApple.signin({
      requestedScopes: [
        ASAuthorizationAppleIDRequest.ASAuthorizationScopeFullName,
        ASAuthorizationAppleIDRequest.ASAuthorizationScopeEmail
      ]
    })
    .then((res: AppleSignInResponse) => {
      // https://developer.apple.com/documentation/signinwithapplerestapi/verifying_a_user
      alert('Send token to apple for verification: ' + res.identityToken);
      console.log("res token ",res);
    })
    .catch((error: AppleSignInErrorResponse) => {
      alert(error.code + ' ' + error.localizedDescription);
      console.error(error);
    });
  }
  notification(){
    this.router.navigate([this.URLConstants.NOTIFICATION]);
  }

}
