import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoginCommonPageRoutingModule } from './login-common-routing.module';

import { LoginCommonPage } from './login-common.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LoginCommonPageRoutingModule
  ],
  declarations: [LoginCommonPage]
})
export class LoginCommonPageModule {}
