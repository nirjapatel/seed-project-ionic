import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginCommonPage } from './login-common.page';

const routes: Routes = [
  {
    path: '',
    component: LoginCommonPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoginCommonPageRoutingModule {}
