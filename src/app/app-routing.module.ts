import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./modules/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'login-common',
    pathMatch: 'full'
  },
  
  {
    path: 'register',
    loadChildren: () => import('./modules/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'login-common',
    loadChildren: () => import('./modules/login-common/login-common.module').then( m => m.LoginCommonPageModule)
  },
  {
    path: 'forget-password',
    loadChildren: () => import('.//modules/forget-password/forget-password.module').then( m => m.ForgetPasswordPageModule)
  },
  {
    path: 'login-otp',
    loadChildren: () => import('./modules/login-otp/login-otp.module').then( m => m.LoginOTPPageModule)
  },
  {
    path: 'notification',
    loadChildren: () => import('./modules/notification/notification.module').then( m => m.NotificationPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
